function cyclic()
	warning('Cyclic Animation test')

  local bg = color_surface(VRESW, VRESH, 10, 10, 10)
	local box = fill_surface(50, 50, 100, 200, 100)

  -- Calculate time based on clockrate
  local time = CLOCKRATE * 1.75

  -- Show the surface
  show_image(bg)
  show_image(box)

  -- Enable cyclic animations for the box
	image_transform_cycle(box, 1)

  -- Put the box on top
  order_image(box, 1)

  -- look ma! No tag_image_transform. That's because 
  -- image_transform_cycle() makes the animation sequential
  move_image(box, VRESW - 50, 0, time, INTERP_EXPOUT)
  move_image(box, VRESW - 50, VRESH - 50, time, INTERP_EXPOUT)
  move_image(box, 0, VRESH - 50, time, INTERP_EXPOUT)
  move_image(box, 0, 0, time, INTERP_EXPOUT)
end

