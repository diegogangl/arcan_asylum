local lifetime = 0
local lain = nil

function shared()
  warning('Shared storage')

  -- Load image
  load_image_asynch('lain.png', function(source, status)
    if status.kind == 'loaded' then
      show_image(source)
    
      -- Create a nulls surface for comparison
      local null = null_surface(VRESW, VRESH)

      print('Do lain and the null share storage?', 
            image_matchstorage(null, source))
    
      -- Share storage between lain image and null
      image_sharestorage(source, null)
      print('Now do lain and the null share storage?', 
            image_matchstorage(null, source))

      lifetime = 50
      lain = source
    end
  end)

end

function shared_clock_pulse()

  -- Count down the lifetime to deletion
  if lifetime > 0 then
    lifetime = lifetime - 1
  end

  -- Delete lain
  if lain and lifetime == 0 then
    delete_image(lain)
    lain = nil
  end
end
