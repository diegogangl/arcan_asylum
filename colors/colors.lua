-- Palette of RGB values for the color blocks
local color_table = {
  {192, 212, 97},
  {174, 247, 142},
  {202, 255, 185},
  {102, 161, 130},
  {46, 64, 87},
}

local bgs = {}

--- Entry point
function colors()
	warning('Colors blocks and looping')
  draw_colors()
end

-- Draw the color blocks
function draw_colors()
  
  -- Calculate height of the color blocks
  local scale_y = VRESH / #color_table

  for item in pairs(color_table) do
    -- Create the surface from the colors at this index
    local bg = color_surface(1920, scale_y, unpack(color_table[item]))

    -- Move the image relative to its position
    nudge_image(bg, 0, (item - 1) * scale_y)

    -- Show the color block
    show_image(bg)
  
    -- Store the vid for the display_state hook
    table.insert(bgs, bg)
  end
end


--- Runs when the display's state changes
--
-- For instance when you resize the window
-- when running Arcan inside X11
function colors_display_state(event)
  local scale_y = VRESH / #color_table
  resize_video_canvas(VRESW, VRESH)

  -- Resize and move the color rows
  for index in pairs(bgs) do
    resize_image(bgs[index], VRESW, scale_y)
    move_image(bgs[index], 0, (index - 1) * scale_y)
  end
end
