local SHADER  =[[
		void main(){
			gl_FragColor = vec4(0.0, 1.0, 0.5, 1.0);
		}
]]

--- Entry point
function basic_shader()
	warning('Basic shader test')

  -- Build fragment shader. The first parameter is the
  -- vertex shader (which we are not using).
  local shid = build_shader(nil, SHADER, 'demo')

  -- Create a surface to attach the shader to
	surface = fill_surface(VRESW, VRESH, 0, 0, 0)

  -- Display the surface
	show_image(surface)

  -- Attach shader to surface
	image_shader(surface, shid)
end
