function hello()

  -- Say hi
	warning('Hello World');
    
  -- Setup the random seed (this is from Lua's math lib)
  math.randomseed(os.time())

  -- Get random red, green, blue values
  local r = math.random(255)
  local g = math.random(255)
  local b = math.random(255)

  -- Create a surface and fill it with a solid color
  local bg = color_surface(1920, 1080, r, g, b)

  -- Show the surface
  show_image(bg)
end

