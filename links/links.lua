function links()
	warning('Linking surfaces together')

  -- Create two boxes, one red and one blue
  local box1 = color_surface(200, 200, 200, 10, 10)
  show_image(box1)

  local box2 = color_surface(200, 200, 10, 10, 200)
  show_image(box2)

  -- Move the boxes
  move_image(box1, 100, 100)
  move_image(box2, 10, 10)
  
  -- Lower opacity to 50% for both boxes
  blend_image(box1, 0.5)
  blend_image(box2, 0.5)
  
  link_image(box1, box2)
  
  -- Try uncommenting to figure out different things

  -- rotate_image(WORLDID, 45)
  -- rotate_image(box1, 45)
  rotate_image(box2, 45)

  -- link_image(box2, box2)
  -- rotate_image(box2, 45)
end

