-- Tralsation table for keyboards. Comes with Arcan
local symtable = system_load("builtin/keyboard.lua")()


--- Entry point
function keys()
	warning('Keyboard test')
  symtable:load_keymap('default.lua')
end


-- Handle input (yeah this includes moving the mouse)
function keys_input(iotbl)
	if (iotbl.kind == 'digital' and iotbl.translated and iotbl.active) then
		sym = symtable[iotbl.keysym]
    print('Pressed', sym)
  end
end
