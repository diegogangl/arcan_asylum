function masks()
	warning('Removing masks to affect animation')

  local center_x = (VRESW / 2) - 200
  local center_y = (VRESH / 2) - 200

  -- Create two boxes, one red and one blue
  local box1 = color_surface(200, 200, 200, 10, 10)
  show_image(box1)

  local box2 = color_surface(200, 200, 10, 10, 200)
  show_image(box2)

  local box3 = color_surface(200, 200, 10, 200, 10)
  show_image(box3)
  
  -- Blend opacity to 1.0 over time
  blend_image(box1, 0)
  blend_image(box2, 0)
  blend_image(box3, 0)
  
  blend_image(box1, 1, 100)
  blend_image(box2, 1, 100)
  blend_image(box3, 1, 100)

  -- Move the boxes
  move_image(box1, 100, 100)
  move_image(box2, 10, 10)
  
  link_image(box1, box2)
  link_image(box2, box3)
  
  rotate_image(box2, 45, 200)

  -- This will transform the entire hierarchy
  move_image(box3, center_x, center_y, 200)
  rotate_image(box3, 45, 200)

  -- Doesn't do much...
  -- image_mask_clear(box3, MASK_LIVING)

  -- This makes box2 and its child visible after expiring box3
  -- image_mask_clear(box2, MASK_LIVING)
  
  -- This won't make box1 visible though, because box2 is 
  -- still being masked
  -- image_mask_clear(box1, MASK_LIVING)

  image_mask_clear(box1, MASK_ORIENTATION)
  image_mask_clear(box1, MASK_OPACITY)

  -- This will expire the entire hierarchy
  expire_image(box3, 180)
end

