function ghosted()
	warning('Removing a box while animating')

  local center_x = (VRESW / 2) - 200
  local center_y = (VRESH / 2) - 200

  -- Create two boxes, one red and one blue
  local box1 = color_surface(200, 200, 200, 10, 10)
  show_image(box1)

  local box2 = color_surface(200, 200, 10, 10, 200)
  show_image(box2)

  local box3 = color_surface(200, 200, 10, 200, 10)
  show_image(box3)

  -- Move the boxes
  move_image(box1, 100, 100)
  move_image(box2, 10, 10)
  
  link_image(box1, box2)
  link_image(box2, box3)
  
  -- This will transform the entire hierarchy
  move_image(box3, center_x, center_y, 200)
  rotate_image(box3, 45, 200)

  -- This will expire the entire hierarchy
  expire_image(box3, 180)
end

