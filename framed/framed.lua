function framed()
	warning('I\'ve been framed!')
 
  -- Create the container box and the surfaces 
  -- that will be the frames
  local box = fill_surface(100, 100, 255, 255, 255)
  local r = fill_surface(100, 100, 255, 0, 0)
  local g = fill_surface(100, 100, 0, 255, 0)
  local b = fill_surface(100, 100, 0, 0, 255)


  -- Tell arcan that box has three more frames
  image_framesetsize(box, 4)

  -- Set the other surfaces as frames
	set_image_as_frame(box, r, 1)
	set_image_as_frame(box, g, 2)
	set_image_as_frame(box, b, 3)

  -- Cycle through frames every 10 ticks
	image_framecyclemode(box, 10)

  show_image(box)
end

