function texcoords()
  warning('Texture coords test')

  -- Load image
  load_image_asynch('lain.png', function(source, status)
    if status.kind == 'loaded' then
      show_image(source)
  
      -- Print the default texture coordinates
      default_txcos = image_get_txcos(source)
      print(table.concat(default_txcos, ","))

      -- Change s,t coords (these are like UV coords)
	    image_set_txcos(source, {-1, -1, 2, -1, 2, 2, -1, 2})
    end
  end)

end

