function chained()
	warning('Chained Animation test')

  local bg = color_surface(VRESW, VRESH, 10, 10, 10)
	local box = fill_surface(50, 50, 100, 200, 100)

  -- Calculate center coordinates from the display size and box size
  local center_w = (VRESW / 2) - (50 / 2)
  local center_h = (VRESH / 2) - (50 / 2)

  -- Calculate time based on clockrate
  local time = CLOCKRATE * 2

  -- Show the surface
  show_image(bg)
  show_image(box)

  -- Put the box on top
  order_image(box, 1)

  -- Move the image to the center over time
  move_image(box, center_w, center_h, time * 1.5, INTERP_EXPOUT)

  -- Run a callback once the first animation is done
	tag_image_transform(box, MASK_POSITION, function()
    print('Finished first anim')

    -- Run another animation with another callback after
    move_image(box, 700, 40, time, INTERP_EXPOUT)

    tag_image_transform(box, MASK_POSITION, function()
      print('Finished second anim')
      move_image(box, 500, 300, time, INTERP_EXPOUT)
	    rotate_image(box, 45, time / 2, INTER_EXPIN)

      tag_image_transform(box, MASK_POSITION, function()
        print('Finished third anim')
      end)
    end)
  end)


  print('Clock rate is', CLOCKRATE)
end

