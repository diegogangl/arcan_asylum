function moving_world()
	warning('Moving the world around')

  -- Create two boxes, one red and one blue
  local box1 = color_surface(200, 200, 200, 10, 10)
  show_image(box1)

  local box2 = color_surface(200, 200, 10, 10, 200)
  show_image(box2)

  -- Move the boxes
  move_image(box1, 100, 100)
  move_image(box2, 10, 10)
  
  -- Lower opacity to 50% for both boxes
  blend_image(box1, 0.5)
  blend_image(box2, 0.5)
  

  -- Transform the world
  -- Try commenting and uncommenting these

  rotate_image(WORLDID, 45) 
  -- move_image(WORLDID, 50, 50)
  -- blend_image(WORLDID, 0.1)
end

