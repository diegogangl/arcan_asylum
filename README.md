# Arcan Asylum

I'm a complete noob in both Arcan and Lua, this repo contains several appls
I've been making to learn different aspects of both. These are all very simple
scripts, but fully commented so I maybe they can also be useful to others.


## Hello  

A hello world kind of thing. Shows a random color every time it is run.

## Hello Again

A less boring hello world, shows a different random color every second
(if it runs at 60fps).

## Mickey

Finally a visible cursor! This is a simple mouse test, left click to turn the
cursor red, right click to turn blue. Double click to turn back to white.

## Colors

Shows a series of colored rows. Involves fun things like loops, moving
surfaces and unpacking tables .


## Guttenberg

Text test. Arcan can do multiple fonts, UTF-8 and even emojis. All on the same
text surface.

## Images

Loading images, validating if loaded correctly, centering and reading props of
the image surface. Also null surfaces for centering and stuff.


## Basic Shader

Sounds exciting, but it's really just a solid color. A _solid_ first step none
the less


## Snapshot

Get a dump of the system in a lua file


## Moving Boxes

First shot at animation, a little green box that moves to the center of the screen.
Suspiciously simple
