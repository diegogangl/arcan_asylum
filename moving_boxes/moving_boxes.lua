function moving_boxes()
	warning('Animation test');

  local bg = color_surface(VRESW, VRESH, 10, 10, 10)
	local box = fill_surface(50, 50, 100, 200, 100)

  -- Calculate center coordinates from the display size and box size
  local center_w = (VRESW / 2) - (50 / 2)
  local center_h = (VRESH / 2) - (50 / 2)

  -- Calculate time based on clockrate
  local time = CLOCKRATE * 5

  -- Show the surface
  show_image(bg)
  show_image(box)

  -- Put the box on top
  order_image(box, 1)

  -- Move the image to the center over time
  move_image(box, center_w, center_h, time)

  print('Clock rate is', CLOCKRATE)
end

