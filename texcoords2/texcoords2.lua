function texcoords2()
  warning('Texture coords test (actually working)')

  -- Load image
  load_image_asynch('lain.png', function(source, status)
    if status.kind == 'loaded' then
      width = status.width
      height = status.height

      half_w = width / 2
      half_h = height / 2

      -- Keep a gap between the surfaces so we can see the effect
      -- Set to zero to see the whole image
      gap = 5
  
      -- Create null surfaces that will be the image's quadrants
      bl = null_surface(width, height)
      tl = null_surface(width, height)
      tr = null_surface(width, height)
      br = null_surface(width, height)

      -- Share source storage to null surfaces
      image_sharestorage(source, bl)
      image_sharestorage(source, tl)
      image_sharestorage(source, tr)
      image_sharestorage(source, br)

      -- Resize each image to be a quadrant
      resize_image(bl, half_w, half_h)
      resize_image(tl, half_w, half_h)
      resize_image(tr, half_w, half_h)
      resize_image(br, half_w, half_h)

      -- Move each quadrant to its position
      move_image(bl, 0 - gap, height - half_h + gap)
      move_image(tl, 0 - gap, 0 - gap)
      move_image(tr, width - half_w + gap, 0 - gap)
      move_image(br, width - half_w + gap, height - half_h + gap)

      -- Change texture coordinates
      -- Coords are clockwise
      -- Default coords are 0,0,  1,0,  1,1,  0,1
	    -- image_set_txcos(bl, {0,0.5,  1,1,  0.5,1,  0,0.5 })
      --
      -- Get a pen and paper if you want to try this, it's 
      -- impossible to figure it out otherwise!
	    image_set_txcos(tl, {0,0,       0.5,0,    0.5,0.5,  0,0.5 })
	    image_set_txcos(tr, {0.5,0,     1,0,      1,0.5,    0.5,0.5 })
	    image_set_txcos(bl, {0,0.5,     0.5,0.5,  0.5,1,    0,1 })
	    image_set_txcos(br, {0.5,0.5,   1,0.5,    1,1,      0.5,1 })

      -- Show all quadrants
      show_image(bl)
      show_image(tl)
      show_image(tr)
      show_image(br)

      -- Print the default texture coordinates
      default_txcos = image_get_txcos(source)
      print(table.concat(default_txcos, ","))
    end
  end)

end

