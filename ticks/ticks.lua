-- Setup the random seed (this is from Lua's math lib)
math.randomseed(os.time())

--- A variable to keep track of current frame
local frame = 0
local bg

--- The drawing function  
function draw_random_box()
  -- Get random red, green, blue values
  local r = math.random(255)
  local g = math.random(255)
  local b = math.random(255)

  -- How many ticks to wait before removing a box
  local life = 500
    
  -- Create a surface and fill it with a solid color
  box = color_surface(250, 250, r, g, b)

  -- Show the surface
  show_image(box)
  move_image(box, math.random(VRESW - 250), 
                  math.random(VRESH - 250))

  expire_image(box, life)
end


--- Entry point
function ticks()
	warning('Clock pulse test')
end


--- Runs every frame
function ticks_clock_pulse()
  frame = frame + 1

  if frame % 2 == 0 then
    draw_random_box()
  end
end


--- Runs when the display's state changes
--
-- For instance when you resize the window
-- when running Arcan inside X11
function ticks_display_state(disp)
  resize_video_canvas(VRESW, VRESH)
end
