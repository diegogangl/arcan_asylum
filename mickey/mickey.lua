-- Create a surface and fill it with a solid color
local bg = color_surface(1920, 1080, 10, 10, 10)


--- Entry point
function mickey()
	warning('Mouse test!')

	warning('Left-click to turn the cursor red')
	warning('Right-click to turn the cursor blue')
	warning('Double click to return to white')

  -- mouse stuff (bundled with Arcan). This is where
  -- all the mouse_* functions live
  system_load("builtin/mouse.lua")()

  -- Order value (should always be MAX_INT I guess?)
  local cursor_layer = 65535

  -- Number of stacked vids to check
  local pick_depth = 1

  -- Cache results of picking to reduce calls
  local cache_pick = true

  -- Start in hidden state
  local hidden = false

  -- Create the vid for the mouse cursor
  local cursor_vid = fill_surface(0, 0, 255, 255, 255)

  -- Setup the mouse handling
  mouse_setup(cursor_vid, cursor_layer, pick_depth, cache_pick, hidden)

  -- Change the mouse scaling (for lols)
  mouse_cursor_sf(2, 2)

  -- Vid for red cursor
  local lclick_vid = fill_surface(0, 0, 255, 0, 0)
	mouse_add_cursor('lclick', lclick_vid, 0, 0)

  -- Vid for blue cursor
  local rclick_vid = fill_surface(0, 0, 0, 0, 255)
	mouse_add_cursor('rclick', rclick_vid, 0, 0)

  -- Add listener for clicks
	mouse_addlistener({
		name = "main",
		own = bg,
		click = lclick_handle,
		rclick = rclick_handle,
		dblclick = dblclick_handle,
	}, {'click', 'rclick', 'dblclick'});
end


-- Handler for left clicking 
function lclick_handle()
  mouse_switch_cursor('lclick')
end


-- Handler for right clicking 
function rclick_handle()
  mouse_switch_cursor('rclick')
end


-- Handler for double clicking (return to default)
function dblclick_handle()
  mouse_switch_cursor('default')
end


--- Runs every frame
function mickey_clock_pulse()

  -- Update the mouse stuff
  mouse_tick(1)

  -- Update visuals
  show_image(bg)
end


-- Handle input (yeah this includes moving the mouse)
function mickey_input(iotbl)
	
  if (iotbl.mouse) then
		mouse_iotbl_input(iotbl)
		return
  end
end


--- Runs when the display's state changes
--
-- For instance when you resize the window
-- when running Arcan inside X11
function mickey_display_state(disp)
  show_image(bg)
end
