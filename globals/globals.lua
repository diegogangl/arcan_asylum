function globals()

  -- Print _G globals table
	warning('Big dump of Arcan globals')
  dump(_G)
    
  local GAP = 60
  local TIME = CLOCKRATE * 10
  local END_X = VRESW - 50

  -- Background image
  local bg = color_surface(VRESW, VRESH, 240, 240, 240)
  show_image(bg)

  -- Since we are doing globals, we can also check out the INTERP_* constants

  -- Create a bunch of boxes to animate
  local box1 = fill_surface(50, 50, 20, 20, 20)
  order_image(box1, 1)
  show_image(box1)

  local box2 = fill_surface(50, 50, 20, 20, 20)
  nudge_image(box2, 0, GAP)
  order_image(box2, 1)
  show_image(box2)

  local box3 = fill_surface(50, 50, 20, 20, 20)
  nudge_image(box3, 0, GAP * 2)
  order_image(box3, 1)
  show_image(box3)

  local box4 = fill_surface(50, 50, 20, 20, 20)
  nudge_image(box4, 0, GAP * 3)
  order_image(box4, 1)
  show_image(box4)

  local box5 = fill_surface(50, 50, 20, 20, 20)
  nudge_image(box5, 0, GAP * 4)
  order_image(box5, 1)
  show_image(box5)

  local box6 = fill_surface(50, 50, 20, 20, 20)
  nudge_image(box6, 0, GAP * 5)
  order_image(box6, 1)
  show_image(box6)

  -- Move all boxes to END_X over TIME using different interpolations
  move_image(box1, END_X, 0, TIME, INTERP_LINEAR)
  move_image(box2, END_X, GAP, TIME, INTERP_EXPIN)
  move_image(box3, END_X, GAP * 2, TIME, INTERP_EXPOUT)
  move_image(box4, END_X, GAP * 3, TIME, INTERP_EXPINOUT)
  move_image(box5, END_X, GAP * 4, TIME, INTERP_SINE)
  move_image(box6, END_X, GAP * 5, TIME, INTERP_SMOOTHSTEP)
end


-- Dump entire table (works for deep tbls)
-- From https://stackoverflow.com/a/42387321
function dump(tbl)
    local checklist = {}
    local function innerdump( tbl, indent )
        checklist[ tostring(tbl) ] = true
        for k,v in pairs(tbl) do
            print(indent..k,v,type(v),checklist[ tostring(tbl) ])
            if (type(v) == "table" and not checklist[ tostring(v) ]) then innerdump(v,indent.."    ") end
        end
    end
    checklist[ tostring(tbl) ] = true
    innerdump( tbl, "" )
end

