function clipping()
	warning('Clipping')

  -- Create two boxes, one red and one blue
  local box1 = color_surface(200, 200, 200, 10, 10)
  show_image(box1)

  local box2 = color_surface(200, 200, 10, 10, 200)
  show_image(box2)

  local box3 = color_surface(200, 200, 10, 200, 10)
  show_image(box3)

  -- Move the boxes
  move_image(box1, 100, 100)
  move_image(box2, 75, 75)
  move_image(box3, 50, 50)
  
  -- Lower opacity to 50% for both boxes
  blend_image(box1, 0.5)
  blend_image(box2, 0.5)
  
  -- Parent the boxes
  link_image(box1, box2)
  link_image(box2, box3)

  -- image_clip_on(box1, CLIP_ON)
  image_clip_on(box1, CLIP_SHALLOW)
end

