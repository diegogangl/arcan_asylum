-- Setup the random seed (this is from Lua's math lib)
math.randomseed(os.time())

--- A variable to keep track of current frame
local frame = 0

local bg

--- The drawing function  
function draw_random_color()
  -- Get random red, green, blue values
  local r = math.random(255)
  local g = math.random(255)
  local b = math.random(255)
    
  -- Create a surface and fill it with a solid color
  bg = color_surface(1920, 1080, r, g, b)

  -- Show the surface
  show_image(bg)
end


--- Entry point
function hello_again()
	warning('You again...')
  draw_random_color()
end


--- Runs every frame
function hello_again_clock_pulse()
  frame = frame + 1

  if frame % 60 == 0 then
    draw_random_color()
  end
end


--- Runs when the display's state changes
--
-- For instance when you resize the window
-- when running Arcan inside X11
function hello_again_display_state(disp)
  resize_video_canvas(VRESW, VRESH)
end
