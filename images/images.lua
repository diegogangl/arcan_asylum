-- Load image file
local img = load_image('black_lagoon.jpg')

-- Null surface only used to center the image
local null_bg = null_surface(VRESW, VRESH)


--- Entry point
function images()
	warning('Loading images')

  -- Check if the image could be loaded succesfully
  if valid_vid(img) then
    -- Center the image using the null surface
    center_image(img, null_bg)

    -- Get image surface props
    local props = image_surface_properties(img)
    print('Image information')
    print('x ', props.x)
    print('y', props.y)
    print('height', props.height)
    print('width', props.width)

    -- Show image
    show_image(img)
  end
end


--- Runs when the display's state changes
--
-- For instance when you resize the window
-- when running Arcan inside X11
function images_display_state(event)
  show_image(img)
end
