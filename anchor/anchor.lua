function anchor()

	warning('Anchor test')

  -- Create a surface and fill it with a solid color
  local bg = color_surface(VRESW, VRESH, 10, 10, 10)
  show_image(bg)

  -- Green box
  local box = color_surface(100, 100, 0, 200, 0)
  show_image(box)

  -- Create four red boxes
  local tl = color_surface(10, 10, 200, 0, 0)
  local bl = color_surface(10, 10, 200, 0, 0)
  local tr = color_surface(10, 10, 200, 0, 0)
  local br = color_surface(10, 10, 200, 0, 0)

  show_image(tl)
  show_image(bl)
  show_image(tr)
  show_image(br)

  -- Link them at the four corners of the green box
  link_image(tl, box, ANCHOR_UL)
  link_image(bl, box, ANCHOR_LL)
  link_image(tr, box, ANCHOR_UR)
  link_image(br, box, ANCHOR_LR)

  -- Center, resize to see how it reacts
  center_image(box, bg)
  resize_image(box, 200, 200, 100)
end
