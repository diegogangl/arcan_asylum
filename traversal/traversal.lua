function traversal()
	warning('Finding families')

  -- Create two boxes, one red and one blue
  local box1 = color_surface(200, 200, 200, 10, 10)
  show_image(box1)

  local box2 = color_surface(200, 200, 10, 10, 200)
  show_image(box2)

  local box3 = color_surface(200, 200, 10, 200, 10)
  show_image(box3)

  -- Move the boxes
  move_image(box1, 100, 100)
  move_image(box2, 10, 10)
  move_image(box3, 50, 50)
  
  -- Lower opacity to 50% for both boxes
  blend_image(box1, 0.5)
  blend_image(box2, 0.5)
  
  -- Parent the boxes
  link_image(box1, box2)
  link_image(box2, box3)

  -- Give them names
  image_tracetag(box1, 'Red box')
  image_tracetag(box2, 'Blue box')
  image_tracetag(box3, 'Green box')

  -- Test the print functions
  print_children(box3)
  print_parent(box2)
end


-- Prints a list of children for a vid
function print_children(vid)
	children = image_children(vid)
  name =  image_tracetag(vid)

  print('-------------------------------------------------------------------------------')
  print('Children of', name)

	for i,v in ipairs(children) do
		print(image_tracetag(v))
	end

  print('-------------------------------------------------------------------------------')
  print('\n')
end

-- Prints a vid's parent 
function print_parent(vid)
	parent = image_parent(vid)
  name =  image_tracetag(vid)

  print('-------------------------------------------------------------------------------')
  print('Parent of', name)
  print(image_tracetag(parent))

  print('-------------------------------------------------------------------------------')
  print('\n')
end
