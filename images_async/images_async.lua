-- Null surface only used to center the image
local null_bg = null_surface(VRESW, VRESH)

print('Before loading...')

load_image_asynch('black_lagoon.jpg', function(source, status)
  if status.kind == 'loaded' then
    print('Image loaded!')
    center_image(source, null_bg)
    show_image(source)
  end
end)

print('After loading...')

--- Entry point
function images_async()
	warning('Loading images asynchronously')
end

