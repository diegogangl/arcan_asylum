
local bg
local txt

--- Entry point
function guttenberg()
	warning('Text test')
  draw()
end


-- Drawing all the stuff
function draw()

  -- Text table. Odd lines are format strings, even lines are UTF-8
  local message = {
    -- Set the font and size from now on. Also set the color to red.
    '\\fTanugo-TTF-Regular.ttf,21\\#FF5555',
    'Hello World!',

    -- Jump to a new line, and return the carriage (\r) to the
    -- start of the line. Finally, set the color to green
    '\\n\\r\\#55FF55',
    'Hola Mundo!',

    -- Same deal, but with green.
    -- Note that this font supports japanese characters so we
    -- don't have to set another.
    '\\n\\r\\#7777FF',
    'ハローワールド！',

    -- Use the emoji font with a larger size
    '\\n\\r\\femoji.ttf,38',

    --Cheers!
    '🍻️', 
  }

  -- Just a simple background
  bg = color_surface(VRESW, VRESH, 20, 20, 20)

  -- Get a surface with the text
  -- vid -> the text surface
  -- lines -> a table with the text lines
  -- w -> the width of the text surface
  -- h -> the height of the text surface
	local vid, lines, w, h = render_text(message)
  txt = vid

  -- Show both vids
  show_image(bg)
  show_image(vid)

  -- Make sure the text vid is on top
  order_image(vid, 1)

  -- Give it a transparent background
	image_clip_on(vid, CLIP_SHALLOW)

  -- Center text on background (which is the size of the display)
  center_image(vid, bg)

  -- Print the info
  print('Width of text', w)
  print('Height of text', h)
end


--- Runs when the display's state changes
--
-- For instance when you resize the window
-- when running Arcan inside X11
function guttenberg_display_state(event)
  resize_video_canvas(VRESW, VRESH)
  resize_image(bg, VRESW, VRESH)
  center_image(txt, bg)
end
